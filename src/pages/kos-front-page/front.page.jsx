import React from "react";
import MainMenu from "../../components/f-main-menu/f.main-menu.component";
import Frame from '../../components/frame/frame.component';
import "./front.styles.scss";

const FrontPage = () => {
  return (
    <Frame>
      <MainMenu/>
    </Frame>
  );
};

export default FrontPage;
