import React from "react";
import Frame from "../../components/frame/frame.component";
import Footer from "../../components/footer/footer.component";
import Banner from "../../components/banner/banner.component";
import TempImage from "../../assets/banner-2.jpg";
import "./kos-single.styles.scss";
import { Typography, Button } from "@material-ui/core";
const SinglePage = () => {
  return (
    <Frame>
      <div className="section-single-kos">
        <Banner image={TempImage} title="Kos Khayalan" />
        <Typography variant="body1" className="section-single-kos__description">
          <span className="section-single-kos__description--1">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            ultricies condimentum metus, eget vulputate ante convallis ac.
            Curabitur ullamcorper lectus velit, nec vehicula nisi tincidunt non.
            In magna velit, porttitor nec tortor eget, tempus finibus velit.
            Cras ut augue lectus. In ornare urna mi, a molestie sem mollis sed.
            Vestibulum iaculis nibh in velit faucibus sodales. Curabitur lectus
            augue, aliquam nec hendrerit sagittis, fringilla sit amet dui. Sed
            tincidunt luctus leo, non ullamcorper urna faucibus eu. Duis a dui
            dui. Ut in dolor velit. Nulla malesuada placerat tempus. Praesent
            tincidunt lacus erat, nec blandit turpis rhoncus eu. Pellentesque
            rutrum magna ante, non pretium tellus imperdiet sit amet. Praesent
            tempor posuere libero ac pharetra. Curabitur sed mollis urna.
            Pellentesque ornare aliquam tortor non faucibus.
          </span>
          <span className="section-single-kos__description--2">
            Harga : Rp.100000/bulan
          </span>
          <span className="section-single-kos__description--2">
            Telp : 6282233476948
          </span>
        </Typography>
        <Button target="__blank" variant="contained" color="primary" style={{ marginTop: '50px'}} href="https://api.whatsapp.com/send?phone=6282233476938">
          WA Sekarang
        </Button>
      </div>
      <Footer />
    </Frame>
  );
};

export default SinglePage;
