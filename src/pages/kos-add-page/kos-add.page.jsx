import React from "react";
import "./kos-add.styles.scss";
import Frame from "../../components/frame/frame.component";
import { Typography } from "@material-ui/core";
import Footer from "../../components/footer/footer.component";
import KosForms from "../../components/kos-forms/kos-forms.component";
const KosAdd = () => (
  <Frame>
    <React.Fragment>
      <div className="section-add">
        <div className="section-add__banner">
          <Typography variant="h5" className="banner-title">
            Iklankan Kosmu disini !
          </Typography>
        </div>
        <div>
          <KosForms />
        </div>
        <Footer />
      </div>
    </React.Fragment>
  </Frame>
);

export default KosAdd;
