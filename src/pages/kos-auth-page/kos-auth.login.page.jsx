import React from "react";
import { TextField, Button, Typography, CircularProgress } from "@material-ui/core";
import { Link } from "react-router-dom";
import { Field, reduxForm } from "redux-form";
import { connect } from 'react-redux';
import { loginKos } from '../../redux/kos-card/card.actions';
import FrameAuth from "../../components/frame-auth/frame.component";
import Banner3 from "../../assets/banner-3.png";
const RenderInput = ({ input, label, placeholder, type, classes }) => (
  <TextField
    label={label}
    placeholder={placeholder}
    type={type}
    InputLabelProps={{ shrink: true }}
    className={`input-field ${classes}`}
    {...input}
  />
);
const LoginAuth = ({ loginKos, handleSubmit }) => {
  const [visibility, setVisibility] = React.useState(false);
  const onSubmit = formValues => {
    setVisibility(true);
    setTimeout(() => {
      setVisibility(false)
    }, 3000);
    loginKos(formValues);
  }
  return (
    <FrameAuth banner={Banner3} title="SignIn to continue">
      <form className="section-input__text-field" onSubmit={handleSubmit(onSubmit)}>
        <Field
          name="email"
          label="Email"
          placeholder="aku@gmail.com"
          type="text"
          classes="text-field--1"
          component={RenderInput}
        />
        <Field
          name="password"
          label="Password"
          placeholder="Password"
          type="password"
          classes="text-field--2"
          component={RenderInput}
        />
        <Button type="submit" variant="contained" color="primary" style={{ marginTop: 30 }}>
          {visibility ? <CircularProgress size={20} style={{ color: 'white'}}/> : 'Masuk'}
        </Button>
        <Typography
          variant="subtitle2"
          style={{ fontWeight: "300", marginTop: "10px" }}
        >
          Don't have any account yet ? Register <Link to="/register">here</Link>
        </Typography>
      </form>
    </FrameAuth>
  );
};

const formHOC = reduxForm({
  form: "Login"
})(LoginAuth);

export default connect(null, { loginKos })(formHOC);
