import React from "react";
import "./f.main-menu.styles.scss";
import Banner from "../f-banner-search/f.banner.component";
import { Typography } from "@material-ui/core";
import Footer from '../footer/footer.component';
const MainMenu = () => {
  return (
    <div className="main-menu">
      <Banner />
      <div className="main-menu-component">
        <Typography variant="h6" className="component-title">
          Cari di wilayah ini
        </Typography>
        <div className="component-menu">
          <div className="component-menu__item-1">
            <Typography variant="h5" className="component-menu__item-1--1">
              Sawojajar
            </Typography>
            <Typography variant="body1" className="component-menu__item-1--2">
              Cari Kosmu disini
            </Typography>
          </div>
          <div className="component-menu__item-2">
            <Typography variant="h5" className="component-menu__item-2--1">
              Kepanjen
            </Typography>
            <Typography variant="body1" className="component-menu__item-2--2">
              Cari Kosmu disini
            </Typography>
          </div>
          <div className="component-menu__item-3">
            <Typography variant="h5" className="component-menu__item-2--1">
              Sukun
            </Typography>
            <Typography variant="body1" className="component-menu__item-2--2">
              Cari Kosmu disini
            </Typography>
          </div>
          <div className="component-menu__item-4">
            <Typography variant="h5" className="component-menu__item-2--1">
              Singosari
            </Typography>
            <Typography variant="body1" className="component-menu__item-2--2">
              Cari Kosmu disini
            </Typography>
          </div>
          <div className="component-menu__item-5">
            <Typography variant="h5" className="component-menu__item-2--1">
              Blimbing
            </Typography>
            <Typography variant="body1" className="component-menu__item-2--2">
              Cari Kosmu disini
            </Typography>
          </div>
          <div className="component-menu__item-6">
            <Typography variant="h5" className="component-menu__item-2--1">
              Klojen
            </Typography>
            <Typography variant="body1" className="component-menu__item-2--2">
              Cari Kosmu disini
            </Typography>
          </div>
        </div>
        <Footer/>
      </div>
    </div>
  );
};

export default MainMenu;
