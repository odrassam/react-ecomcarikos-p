import React from "react";
import "./snackbar.styles.scss";
import { Snackbar, IconButton, SnackbarContent } from "@material-ui/core";
import { Error, Close } from "@material-ui/icons";
import { connect } from 'react-redux';
import { closeError } from '../../redux/kos-card/card.actions';
const SnackbarComponent = ({ closeError, error, message }) => {
  console.log(error)
  setTimeout(() => {
    closeError();
  }, 5000);
  return (
    <Snackbar
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      open={error}
    >
      <SnackbarContent
        message={
          <span
            id="client-snackbar"
            style={{ display: "flex", alignItems: "center" }}
          >
            <Error />
            <span style={{ marginLeft: "20px", fontSize: 17 }}>{message}</span>
          </span>
        }
        action={[
          <IconButton key="close" aria-label="close" color="inherit" onClick={closeError}>
            <Close />
          </IconButton>
        ]}
        style={{ backgroundColor: "#D32F2F" }}
      />
    </Snackbar>
  );
};
const mapStateToProps = state => ({
  error: state.kosCard.error
})
export default connect(mapStateToProps, { closeError })(SnackbarComponent);
