import React from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TemporaryImg from "../../assets/lokasi-1.jpg";
import { Link } from "react-router-dom";
import "./card.styles.scss";
const CardItem = () => {
  return (
    <div className="card-item">
      <Card style={{ maxWidth: 340, height: 350 }}>
        <CardActionArea>
          <Link to="/kos/pilih/1" style={{ textDecoration: "none" }}>
            <CardMedia
              image={TemporaryImg}
              title="Contemplative Reptile"
              style={{ height: 150 }}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Kos Bunda
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Kos murah meriah, ayo langsung negekos disini !
              </Typography>
            </CardContent>
          </Link>
        </CardActionArea>
        <CardActions>
          <Link to="/kos/pilih/1" style={{ textDecoration: 'none'}}>
            <Button size="small" color="secondary">
              See this kosan
            </Button>
          </Link>
        </CardActions>
      </Card>
    </div>
  );
};

export default CardItem;

