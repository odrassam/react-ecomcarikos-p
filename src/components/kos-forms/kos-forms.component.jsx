import React from "react";
import { TextField, Button, CircularProgress } from "@material-ui/core";
import { Field, reduxForm } from "redux-form";
import ImageUploader from "../image-uploader/image.component";
import "./kos-forms.styles.scss";
import { connect } from "react-redux";
import { createKosan } from "../../redux/kos-card/card.actions";

const renderInput = ({ input, label, css, placeholder, type = "text" }) => {
  return (
    <TextField
      variant="outlined"
      required
      label={label}
      placeholder={placeholder}
      InputLabelProps={{
        shrink: true
      }}
      className={css}
      {...input}
      type={type}
    />
  );
};
const renderTextArea = ({ input, label, css, placeholder }) => {
  return (
    <TextField
      variant="outlined"
      label={label}
      placeholder={placeholder}
      InputLabelProps={{
        shrink: true
      }}
      className={css}
      {...input}
      multiline={true}
      rows={2}
      rowsMax={Infinity}
      required
    />
  );
};
const KosForms = ({ createKosan, handleSubmit }) => {
  const [visibility, setVisibility] = React.useState(false);
  const onSubmit = formValues => {
    createKosan(formValues);
    setVisibility(true);
    setTimeout(() => {
      setVisibility(false);
    }, 3000);
  };
  return (
    <React.Fragment>
      <form className="section-add__fields" onSubmit={handleSubmit(onSubmit)}>
        <Field
          name="nama_kos"
          component={renderInput}
          placeholder="Kos Cantik"
          label="Nama Kosanmu !"
          css="field-1"
        />
        <Field
          name="harga_kos"
          component={renderInput}
          placeholder="100000"
          label="Harga Kosanmu/bulan"
          css="field-2"
          type="number"
        />
        <Field
          name="no_telp"
          component={renderInput}
          placeholder="0822********"
          label="Nomor Hp/WA"
          css="field-2"
          type="number"
        />
        <Field
          name="tentang_kos"
          component={renderTextArea}
          placeholder="Kosku......."
          label="Deskripsi Kosanmu !"
          css="field-3"
        />
        <ImageUploader />
        <Button
          type="submit"
          style={{ marginBottom: "20px" }}
          color="primary"
          variant="contained"
        >
          {visibility ? <CircularProgress size={20} style={{ color: 'white'}}/> : 'Publikasi Kosmu !'}
        </Button>
      </form>
    </React.Fragment>
  );
};

const FormHOC = reduxForm({
  form: "KosForm"
})(KosForms);
const mapStateToProps = state => ({
  result: state.kosCard
});
export default connect(
  mapStateToProps,
  { createKosan }
)(FormHOC);
