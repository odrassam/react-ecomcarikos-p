import React from "react";
import "./f.side-menu.styles.scss";
import { Typography, Icon, ButtonBase } from "@material-ui/core";
import HomeWorkIcon from "@material-ui/icons/HomeWork";
import { Link } from "react-router-dom";

const SideMenu = () => {
  return (
    <div className="side-menu">
      <Link to="/" style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <Icon className="side-menu--item__icon">dashboard</Icon>
          <Typography variant="h5" className="side-menu--item__title">
            Home
          </Typography>
        </ButtonBase>
      </Link>
      <Link to="/kos/pilih" style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <Icon className="side-menu--item__icon">search</Icon>
          <Typography variant="h5" className="side-menu--item__title">
            Cari Kos
          </Typography>
        </ButtonBase>
      </Link>
      <Link to="/kos/tambah/1" style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <Icon className="side-menu--item__icon">add_to_photos</Icon>
          <Typography variant="h5" className="side-menu--item__title">
            Buat Kos
          </Typography>
        </ButtonBase>
      </Link>
      <Link to="/kos/owned/1" style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <HomeWorkIcon className="side-menu--item__icon" />
          <Typography variant="h5" className="side-menu--item__title">
            Kosku
          </Typography>
        </ButtonBase>
      </Link>
      <ButtonBase disableRipple={true} className="side-menu--item">
        <Icon className="side-menu--item__icon">exit_to_app</Icon>
        <Typography variant="h5" className="side-menu--item__title">
          Logout
        </Typography>
      </ButtonBase>
    </div>
  );
};

export default SideMenu;
