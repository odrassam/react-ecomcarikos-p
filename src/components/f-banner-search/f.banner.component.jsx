import React from "react";
import "./f.banner.styles.scss";
import BannerImg from "../../assets/banner.png";
import { Typography,  IconButton, Icon } from "@material-ui/core";

const Banner = () => (
  <div className="banner-front">
    <img src={BannerImg} alt="Banner.png" className="banner-img" />
    <div className="banner-header">
      <Typography variant="h5" className="banner-title">
        Cari Kos
      </Typography>
      <div className="banner-input">
        <IconButton className="banner-input__icon">
          <Icon>search</Icon>
        </IconButton>
        <input
          className="banner-input__field"
          placeholder="Cari Kos Region Malang"
        />
      </div>
    </div>
  </div>
);

export default Banner;
