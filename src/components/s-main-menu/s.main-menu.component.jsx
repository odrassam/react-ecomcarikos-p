import React from "react";
import "./s.main-menu.styles.scss";
import { Typography } from "@material-ui/core";
import CardItem from "../s-card-item/card.item.component";
const MainMenu = () => {
  return (
    <div className="s-main-menu">
      <Typography className="card-title" variant="h6">
        Kepanjen
      </Typography>
      <div className="card-content">
        <CardItem/>
        <CardItem/>
        <CardItem/>
        <CardItem/>
      </div>
    </div>
  );
};

export default MainMenu;
