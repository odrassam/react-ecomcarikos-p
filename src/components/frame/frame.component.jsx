import React from "react";
import "./frame.styles.scss";
import Header from "../header/header.component";
import SideMenu from "../f-side-menu/f.side-menu.component";
import Snackbar from '../snackbar/snackbar.component';
const Frame = props => (
  <React.Fragment>
    <Header />
    <Snackbar message="Banner belum diinputkan !"/>
    <div className="front-page-content">
      <div className="front-page-content__side-menu">
        <SideMenu />
      </div>
      <div className="front-page-content__main-menu">{props.children}</div>
    </div>
  </React.Fragment>
);

export default Frame;
