import React from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import { ListItemText, Icon } from "@material-ui/core";
import "./menu.styles.scss";

export default function SimpleMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }
  function handleClose() {
    setAnchorEl(null);
  }
  return (
    <div>
      <div className="app-bar__popup">
        <IconButton
          onClick={handleClick}
          edge="start"
          style={{ padding: "7px" }}
        >
          <Avatar>A</Avatar>
        </IconButton>
        <div className="app-bar__avatar-name">
          <Typography variant="body2">Bagus Ridho </Typography>
        </div>
      </div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        style={{ marginTop: '40px'}}
      >
        <MenuItem onClick={handleClose}  divider>
          <Avatar>A</Avatar>
          <ListItemText style={{ paddingLeft: '10px'}} primary={"Bagus Ridho"} secondary={"@kisraabrawis"} />
        </MenuItem>
        <MenuItem onClick={handleClose} className="pdl-small" style={{ marginTop: 5}}>
          <Icon>account_circle</Icon>
          <ListItemText className="pdl-small" primary="Profile saya"/>
        </MenuItem>
        <MenuItem className="pdl-small" onClick={handleClose} style={{ marginTop: 5 }}>
          <Icon>vpn_key</Icon>
          <ListItemText className="pdl-small" primary="Ubah Password" />
        </MenuItem>
        <MenuItem className="pdl-small" onClick={handleClose} style={{ marginTop: 5 }}>
          <Icon>exit_to_app</Icon>
          <ListItemText className="pdl-small" primary="Logout" />
        </MenuItem>
      </Menu>
    </div>
  );
}
