import React from 'react';
import './App.styles.scss';
import { Router, Route } from 'react-router-dom';
import history from './History';
import FrontPage from './pages/kos-front-page/front.page';
import KosListPage from './pages/kos-list-page/kos.list';
import KosTambahPage from './pages/kos-add-page/kos-add.page';
import KosSinglePage from './pages/kos-single-page/kos-single.page';
import KosOwnedPage from './pages/kos-owned-page/kos.owned.page';
import KosLoginPage from './pages/kos-auth-page/kos-auth.login.page';
import KosRegisterPage from './pages/kos-auth-page/kos-auth.register.page';
const App = () => {
  return(
    <Router history={history}>
      <Route path="/" exact component={FrontPage}/>
      <Route path="/kos/pilih" exact component={KosListPage}/>
      <Route path="/kos/pilih/:id" exact component={KosSinglePage}/>
      <Route path="/kos/tambah/:id" exact component={KosTambahPage}/>
      <Route path="/kos/owned/:id" exact component={KosOwnedPage}/>
      <Route path="/login" exact component={KosLoginPage}/>
      <Route path="/register" exact component={KosRegisterPage}/>
    </Router>
  )
}

export default App;