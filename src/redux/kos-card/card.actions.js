import Types from "./card.types";
import Cloudinary from "../api/cloudinary";
import Kosan from "../api/kos";
import history from "../../History";

export const getLocalImage = images => {
  return {
    type: Types.GET_IMAGE_LOCAL,
    payload: images
  };
};
export const setImageData = images => {
  return {
    type: Types.GET_IMAGE,
    payload: images
  };
};
export const registerKos = formValues => async dispatch => {
  try {
    const response = await Kosan.post("/register", formValues, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json"
      }
    });
    dispatch({
      type: Types.REGISTER_KOS,
      payload: response.data
    });
    console.log(response);
    history.push("/login");
  } catch (e) {
    console.log(e.code);
    dispatch({
      type: Types.GET_ERROR,
      payload: true
    });
  }
};
export const loginKos = formValues => async dispatch => {
  try {
    const response = await Kosan.post("/login", formValues, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json"
      }
    });
    dispatch({
      type: Types.LOGIN_KOS,
      payload: response.data
    });
    history.push("/");
  } catch (e) {
    dispatch({
      type: Types.GET_ERROR,
      payload: true
    });
  }
};
export const closeError = () => ({
  type: Types.GET_ERROR,
  payload: false
});
export const createKosan = (formValues, token) => async (
  dispatch,
  getState
) => {
  const { imageData } = getState().kosCard;
  const form = new FormData();
  form.append("file", imageData);
  form.append("upload_preset", "lsvfxa7q");
  try {
    const responseImage = await Cloudinary.post("/upload", form);
    const response = await Kosan.post(
      "/kos",
      { ...formValues, gambar_kos: responseImage.data.secure_url },
      {
        headers: {
          "Authorization": `bearer "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjEwMC45NVwvYXBpXC9sb2dpbiIsImlhdCI6MTU2OTIwMjI1MywiZXhwIjoxNTY5MjA1ODUzLCJuYmYiOjE1NjkyMDIyNTMsImp0aSI6IjRxTUJhbkhQQmxLbk1NSW8iLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.FlCHwrQHCzW_Ge7_JJqMsBMzSEoU0YDtgZEhpHOBpxs`,
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json"
        }
      }
    );
    console.log(response)
    dispatch({
      type: Types.CREATE_KOS,
      payload: response.data
    });
    history.push("/kos/pilih");
  } catch (e) {
    dispatch({
      type: Types.GET_ERROR,
      payload: true
    });
  }
};

export const deleteTrotoar = id => async dispatch => {
  await Kosan.delete(`/trotoar/${id}`);
  dispatch({
    type: Types.DELETE_KOS,
    payload: id
  });
};

export const updateTrotoar = (id, formValues) => async dispatch => {
  const response = await Kosan.patch(`/trotoar/${id}`, formValues);
  dispatch({
    type: Types.UPDATE_KOS,
    payload: response.data
  });
};
