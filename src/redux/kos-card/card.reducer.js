import Types from "./card.types";

const INITIAL_STATE = {
  localImageUrl: null,
  imageData: null,
  kosData: null,
  error: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.GET_IMAGE_LOCAL:
      return {
        ...state,
        localImageUrl: action.payload
      };
    case Types.GET_IMAGE:
      return {
        ...state,
        imageData: action.payload
      };
    case Types.CREATE_KOS:
      return {
        ...state,
        kosData: action.payload,
        localImageUrl: null,
        error: false
      };
    // case Types.UPDATE_KOS:
    //   return { ...state, [action.payload.id]: action.paylaod };
    // case Types.DELETE_KOS:
    //   return { ...state, [action.payload.id]: action.paylaod };
    case Types.GET_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case Types.LOGIN_KOS:
      return action.payload;
    case Types.REGISTER_KOS:
        return action.payload;
    default:
      return state;
  }
};
