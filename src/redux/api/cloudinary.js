import Axios from 'axios';

export default Axios.create({
  baseURL: 'https://api.cloudinary.com/v1_1/smk-telkom-malang/image/'
});