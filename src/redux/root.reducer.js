import { combineReducers } from 'redux';
import { reducer as FormReducer} from 'redux-form';
import KosCardReducer from '../redux/kos-card/card.reducer';
export default combineReducers({
  form: FormReducer,
  kosCard: KosCardReducer,
})